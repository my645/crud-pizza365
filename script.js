$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //Biến chứa Response trả về 
    var vAllListOrderObject = [];
    //Khai báo hằng số :
    const gORDER_ID_COL = 0;
    const gNAME_COL = 1;
    const gEMAIL_COL = 2;
    const gADDRESS_COL = 3;
    const gPHONE_NUMBER_COL = 4;
    const gPIZZA_TYPE_COL = 5;
    const gKICH_CO_COL = 6;
    const gTRANG_THAI_COL = 7;
    const gACTION_COL = 8;
    //Biến mảng hằng số khai báo các thuộc tính datatable
    const gNameCol = ["orderId", "hoTen", "email", "diaChi", "soDienThoai", "loaiPizza", "kichCo", "trangThai", "action"];
    //Khai báo DataTable và mapping columns :
    var vDataTable = $("#table").DataTable({
        columns: [
            { data: gNameCol[gORDER_ID_COL] },
            { data: gNameCol[gNAME_COL] },
            { data: gNameCol[gEMAIL_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gPHONE_NUMBER_COL] },
            { data: gNameCol[gPIZZA_TYPE_COL] },
            { data: gNameCol[gKICH_CO_COL] },
            { data: gNameCol[gTRANG_THAI_COL] },
            { data: gNameCol[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent:
                    `
                <i class="fas fa-thin fa-pen logo-edit" data-toggle="tooltip" data-placement="top" title="Dice"></i>
                <i class="fas fa-thin fa-trash logo-delete" data-toggle="tooltip" data-placement="top" title="Gift"></i>
                `
            }
        ]
    });
    // bạn có thê dùng để lưu trữ combo được chọn, mỗi khi khách chọn bạn lại đổi giá trị properties của nó
    var gSelectedMenuStructure = {
        menuName: "...",    // S, M, L
        duongKinhCM: 0,
        suonNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0,
        discount: 0,
        thanhTien: "",
    }
    //Bạn có thể dùng để lưu loại nước uống được chọn : 
    var vDrinkSelect = "";
    // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
    var gSelectedPizzaType = "";
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //Hàm tải dữ liệu cho select đồ uống :
    loadDataDrinkSelect();
    //Gán sự kiện click cho nút edit
    $(document).on("click", ".logo-edit", function () {
        onBtnEditClick(this);
    });
    //Gán sự kiện click cho nút Lọc Filter
    $(document).on("click", "#btn-filter", function () {
        onBtnFilterClick();
    })
    //Gán sự kiện click cho nút delete 
    $(document).on("click", ".logo-delete", function () {
        onBtnDeleteClick(this);
    });
    //Gán sự kiện click cho nút insert
    $(document).on("click", "#btn-insert", function () {
        onBtnInsertClick();
    });
    //Gán sự kiện click cho nút đồng ý trong modal insert (thêm đơn hàng):
    $(document).on("click", "#btn-accept-insert", function () {
        onBtnAcceptInsertClick()
    });
    //Gán sự kiện on change cho select chọn kích cỡ pizza
    $(document).on("change", "#select-kich-co-pizza", function () {
        getSelectedMenuStructure();
        loadInforMenuToForm();
    });
    //Gán sự kiện on change cho select chọn loại pizza trong form modal :
    $(document).on("change", "#select-loai-pizza-modal", function () {
        getSelectedPizzaType();
    });
    //Gán sự kiện on change cho select chọn đồ uống 
    $(document).on("change", "#select-nuoc-uong", function () {
        getSelectDrink();
    });
    //Gán sự kiện cho nút huỷ bỏ trong modal insert 
    $(document).on("click", "#btn-cancel-insert", function () {
        onBtnCancelInsert();
    });
    //Gán sự kiện cho nút đồng ý trong modal delete :
    $(document).on("click", "#btn-accept-delete", function () {
        onBtnAcceptDeleteClick();
    });
    //Gán sự kiện cho nút huỷ bỏ trong modal delte :
    $(document).on("click", "#btn-cancel-delete", function () {
        onBtnCancelDeleteClick();
    });
    //Gán sự kiện khi bấm nút đồng ý trong modal edit
    $(document).on("click", "#btn-accept-edit", function () {
        onBtnAcceptEditClick();
    });
    //Gán sự kiện khi bấm nút huỷ bỏ trong modal edit 
    $(document).on("click", "#btn-cancel-edit", function () {
        onBtnCancelEditClick();
    });
    //Gán sự kiện khi bấm nút đồng ý trong modal complete
    $(document).on("click", "#btn-thanh-cong", function () {
        onBtnAcceptCompleteClick();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict"

        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            success: function (responseObject) {
                vAllListOrderObject = responseObject;
                console.log(responseObject);
                loadDataToTable(vAllListOrderObject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });
    };
    //Hàm tải dữ liệu cho select đồ uống :
    function loadDataDrinkSelect() {
        "use strict"
        //Gọi sever để lấy danh sách đồ uống :
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            success: function (responseObject) {
                loadDataToSelectDrink(responseObject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });

    }
    //Hàm xử lý sự kiện cho nút Edit
    function onBtnEditClick(paramButton) {
        "use strict"
        //Hiển thị modal lên
        $("#edit-modal").modal("show");
        var vRowClick = $(paramButton).closest("tr");
        var vTable = $("#table").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        var vId = vDataRow.id;
        console.log(vDataRow);
        console.log(vId);
        $("#inp-ma-don-hang").val(vId);
        $("#select-trang-thai-don-hang").val(vDataRow.trangThai);
    };
    //Hàm xử lý sự kiện khi bấm nút Filter 
    function onBtnFilterClick() {
        "use strict"
        //Bước 1 : Tạo object để thu thập dữ liệu  :
        var vFilterObject = {
            trangThai: "",
            loaiPizza: "",
        }
        //Bước 2: Thu thập dữ liệu :
        vFilterObject.trangThai = $("#select-trang-thai option:checked").val();
        vFilterObject.loaiPizza = $("#select-loai-pizza option:checked").val();
        //Bước 3: Kiểm tra dữ liệu :(không cần)
        //Bước 4: Lọc filter 
        getFilterObject(vFilterObject);
    };
    //Hàm xử lý sự kiện khi bấm bút Insert
    function onBtnInsertClick() {
        "use strict"
        $("#insert-modal").modal("show");
    }
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal thêm đơn hàng 
    function onBtnAcceptInsertClick() {
        "use strict"
        //Bước 1: Tạo object để thu thập dữ liệu đơn hàng :
        var vObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",
        };
        //Bước 2 : Thu thập dữ liệu :
        getDataOrder(vObjectRequest);
        //Thanh toán thành tiền dựa vào mã voucher đã nhập:
        getDiscountByVoucherId(vObjectRequest);
        console.log(vObjectRequest)
        //Bước 3: Kiểm tra dữ liệu :
        if (validateDataOrder(vObjectRequest)) {
            // Bước 4 : Gọi sever :
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                type: "POST",
                data: JSON.stringify(vObjectRequest),
                contentType: "application/json",
                success: function (responseObject) {
                    console.log(responseObject);
                    //Sau khi thêm đơn hàng thành công thì :
                    //Reset lại vùng nhập thông tin modal:
                    resetFormModalInsert();
                    //Tắt modal insert đi:
                    $("#insert-modal").modal("hide");
                    //Hiện modal cập nhật đơn hàng thành công
                    $("#complete-modal").modal("show");
                    loadInforMenuToFormModalComplete(responseObject);
                    //Lấy lại danh sách order và load lại bảng :
                    onPageLoading();
                },
                error: function (ajaxContext) {
                    alert(ajaxContext.responseText)
                }
            });
        }
    };
    //Hàm xử lý sư kiện khi bấm nút huỷ bỏ trong modal insert 
    function onBtnCancelInsert() {
        "use strict"
        $("#insert-modal").modal("hide");
        resetFormModalInsert();
    };
    //Hàm xử lý sự kiện khi bấm nút delete 
    function onBtnDeleteClick(paramButton) {
        "use strict"
        $("#delete-modal").modal("show");
        var vRowClick = $(paramButton).closest("tr");
        var vTable = $("#table").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        var vId = vDataRow.id;
        $("#span").html(vId);

    };
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal delete
    function onBtnAcceptDeleteClick() {
        "use strict"
        var vId = $("#span").html();
        console.log(vId);
        //Gọi sever :
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + vId,
            async: true,
            type: "DELETE",
            success: function (responseObject) {
                alert("xoá đơn hàng thành công");
                //Ẩn modal đi 
                $("#delete-modal").modal("hide");
                //Lấy lại danh sách đơn hàng và load lại bảng
                onPageLoading();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });
    };
    //hàm xử lý sự kiện khi bấm nút huỷ bỏ trong modal delete
    function onBtnCancelDeleteClick() {
        "use strict"
        //Ẩn modal đi: 
        $("#delete-modal").modal("hide");
    };
    //Hàm xử lý sự kiện khi bấm nút đồng ý trong modal edit 
    function onBtnAcceptEditClick() {
        "use strict"
        //Bước 1 : Tạo object thu thập dữ liệu 
        var vId = $("#inp-ma-don-hang").val();
        var vObjectRequest = {
            trangThai: ""
        };
        //Bước 2 : Thu thập dữ liệu :
        getObjectRequest(vObjectRequest);
        //Bước 3 : Kiểm tra dữ liệu :
        if (validateObjectRequest(vObjectRequest)) {
            //Bước 4: Gọi sever để update trạng thái đơn hàng :
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + vId,
                data: JSON.stringify(vObjectRequest),
                type: "PUT",
                contentType: "application/json",
                success: function (responseObject) {
                    console.log(responseObject);
                    //Update thông tin thành công thì :
                    alert("Update thông tin đơn hàng thành công!");
                    //Ẩn modal đi :
                    $("#edit-modal").modal("hide");
                    //Lấy lại danh sách đơn hàng và load lại bảng :
                    onPageLoading();
                },
                error: function (ajaxContext) {
                    alert(ajaxContext.responseText)
                }
            });
        }

    };
    //hàm xử lý sự kiện khi bấm nút huỷ bỏ trong modal edit
    function onBtnCancelEditClick() {
        "use strict"
        $("#edit-modal").modal("hide");
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm tải dữ liệu cho bảng :
    function loadDataToTable(paramObject) {
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramObject);
        vDataTable.draw();
    };
    //Hàm tải thông tin đơn hàng lên form modal 
    function loadDataRowToFormModal(paramDataRow) {
        "use strict"

    }
    //Hàm lọc dữ liệu :
    function getFilterObject(paramFilterObject) {
        "use strict"
        var vFilteredObject = [];
        //Trường hợp 1 : Chỉ lọc trạng thái , không lọc loại pizza :
        if (paramFilterObject.trangThai !== "" || paramFilterObject.loaiPizza == "") {
            vFilteredObject = vAllListOrderObject.filter(function (paramObject) {
                return (paramObject.trangThai === paramFilterObject.trangThai)
            });
        };
        //Trường hợp 2 : Chỉ lọc loại pizza , không lọc trạng thái :
        if (paramFilterObject.loaiPizza !== "" || paramFilterObject.trangThai == "") {
            vFilteredObject = vAllListOrderObject.filter(function (paramObject) {
                return (paramObject.loaiPizza === paramFilterObject.loaiPizza)
            });
        };
        //Trường hợp 3 : Lọc cả trạng thái pizza và trạng thái 
        if (paramFilterObject.loaiPizza !== "" || paramFilterObject.trangThai !== "") {
            vFilteredObject = vAllListOrderObject.filter(function (paramObject) {
                return (paramObject.loaiPizza === paramFilterObject.loaiPizza || paramObject.trangThai === paramFilterObject.trangThai);
            });
        };
        //Trường hợp 4 : Không lọc gì cả
        if (paramFilterObject.loaiPizza == "" || paramFilterObject.trangThai == "") {
            vFilteredObject = vAllListOrderObject;
        };
        loadDataToTable(vFilteredObject);
    };
    //Hàm lấy dữ liệu cho combo pizza
    function getSelectedMenuStructure() {
        "use strict"
        var vSelectedSizePizza = $("#select-kich-co-pizza option:checked").val();
        console.log(vSelectedSizePizza);
        if (vSelectedSizePizza === "S") {
            //Đổi giá trị gSelectedMenuStructure :
            gSelectedMenuStructure.menuName = "S";
            gSelectedMenuStructure.duongKinhCM = 20;
            gSelectedMenuStructure.suonNuong = 2;
            gSelectedMenuStructure.saladGr = 200;
            gSelectedMenuStructure.drink = 2;
            gSelectedMenuStructure.priceVND = 150000;
        };
        if (vSelectedSizePizza === "M") {
            //Đổi giá trị gSelectedMenuStructure :
            gSelectedMenuStructure.menuName = "M";
            gSelectedMenuStructure.duongKinhCM = 25;
            gSelectedMenuStructure.suonNuong = 4;
            gSelectedMenuStructure.saladGr = 300;
            gSelectedMenuStructure.drink = 3;
            gSelectedMenuStructure.priceVND = 200000;
        };
        if (vSelectedSizePizza === "L") {
            //Đổi giá trị gSelectedMenuStructure :
            gSelectedMenuStructure.menuName = "L";
            gSelectedMenuStructure.duongKinhCM = 30;
            gSelectedMenuStructure.suonNuong = 8;
            gSelectedMenuStructure.saladGr = 500;
            gSelectedMenuStructure.drink = 4;
            gSelectedMenuStructure.priceVND = 250000;
        };
        console.log(gSelectedMenuStructure);
    };
    //Hàm lấy dữ liệu loại pizza 
    function getSelectedPizzaType() {
        "use strict"
        var vSelectedTypePizza = $("#select-loai-pizza-modal option:checked").val();
        gSelectedPizzaType = vSelectedTypePizza;
        console.log(gSelectedPizzaType);
    }
    //Hàm tải dữ liệu cho select đồ uống
    function loadDataToSelectDrink(paramObject) {
        "use strict"
        //Truy xuất thẻ element select đồ uống :
        var vSelectDrink = $("#select-nuoc-uong");
        for (var vIndex = 0; vIndex < paramObject.length; vIndex++) {
            $("<option/>")
                .val(paramObject[vIndex].maNuocUong)
                .text(paramObject[vIndex].tenNuocUong)
                .appendTo(vSelectDrink);
        };
    }
    //Hàm lấy dữ liệu đồ uống :
    function getSelectDrink() {
        "use strict"
        var vSelectedDrink = $("#select-nuoc-uong option:checked").val();
        vDrinkSelect = vSelectedDrink;
        console.log(vDrinkSelect);
    }
    //Hàm lấy dữ liệu cho order 
    function getDataOrder(paramObjectRequest) {
        "use strict"
        paramObjectRequest.kichCo = gSelectedMenuStructure.menuName;
        paramObjectRequest.duongKinh = gSelectedMenuStructure.duongKinhCM;
        paramObjectRequest.suon = gSelectedMenuStructure.suonNuong;
        paramObjectRequest.salad = gSelectedMenuStructure.saladGr;
        paramObjectRequest.loaiPizza = gSelectedPizzaType;
        paramObjectRequest.idVourcher = $("#inp-ma-giam-gia").val().trim();
        paramObjectRequest.idLoaiNuocUong = vDrinkSelect;
        paramObjectRequest.soLuongNuoc = gSelectedMenuStructure.drink;
        paramObjectRequest.hoTen = $("#inp-ho-ten").val().trim();
        paramObjectRequest.email = $("#inp-email").val().trim();
        paramObjectRequest.soDienThoai = $("#inp-so-dien-thoai").val().trim();
        paramObjectRequest.diaChi = $("#inp-dia-chi").val().trim();
        paramObjectRequest.loiNhan = $("#inp-loi-nhan").val().trim();

    };
    //Hàm lấy phần trăm giảm giá từ voucher id 
    function getDiscountByVoucherId(paramRequestObject) {
        "use strict"
        if (paramRequestObject.idVourcher !== "") {
            //Call Ajax để lấy phần trăm giảm giá 
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramRequestObject.idVourcher,
                async: false,
                type: "GET",
                success: function (responseObject) {
                    gSelectedMenuStructure.discount = responseObject.phanTramGiamGia;
                    console.log(responseObject);
                    //Hiển thị mã orderId lên form modal complete
                },
                error: function (ajaxContext) {
                    //Vì một lý do nào đó mã giảm giá không được nhập thì phần trăm giảm giá sẽ bằng 0
                    console.log("Không tìm thấy mã voucher");
                }
            });
        }
        //Nếu không nhập giá trị mã giảm giá thì phần trăm giảm giá bằng 0
        else {
            gSelectedMenuStructure.discount = 0;
        }
        //Giá trị thành tiền = Giá ban đầu * ( 1 - Phần trăm giảm giá / 100 )
        paramRequestObject.thanhTien = gSelectedMenuStructure.priceVND * (1 - gSelectedMenuStructure.discount / 100);
    }
    //Hàm kiểm tra dữ liệu 
    function validateDataOrder(paramObjectRequest) {
        "use strict"
        if (paramObjectRequest.hoTen === "") {
            alert("Hãy điền họ tên");
            return false;
        };
        if (paramObjectRequest.email === "") {
            alert("Hãy điền email");
            return false;
        };
        if (paramObjectRequest.email.includes("@") == false) {
            alert("Email điền vào không hợp lệ");
            return false;
        };
        if (paramObjectRequest.soDienThoai === "") {
            alert("Hãy điền số điện thoại");
            return false;
        };
        if (isNaN(paramObjectRequest.soDienThoai) === true) {
            alert("Số điện thoại nhập vào phải là một số");
            return false;
        };
        if (paramObjectRequest.diaChi === "") {
            alert("Hãy điền địa chỉ");
            return false;
        };
        if (paramObjectRequest.idLoaiNuocUong === "") {
            alert("Hãy chọn loại nước uống");
            return false;
        };
        if (paramObjectRequest.kichCo === "...") {
            alert("Hãy chọn size pizza");
            return false;
        };
        if (paramObjectRequest.loaiPizza === "") {
            alert("Hãy chọn loại pizza");
            return false;
        };
        return true;
    };
    //Hàm xoá trắng dữ liệu trên form :
    function resetFormModalInsert() {
        "use strict"
        $("#inp-ho-ten").val("");
        $("#inp-email").val("");
        $("#inp-so-dien-thoai").val("");
        $("#inp-dia-chi").val("");
        $("#inp-ma-giam-gia").val("");
        $("#inp-loi-nhan").val("");
        $("#select-nuoc-uong").val("");
        $("#select-kich-co-pizza").val("");
        $("#select-loai-pizza-modal").val("");
        $("#inp-duongKinh").val("");
        $("#inp-suon").val("");
        $("#inp-soLuongNuoc").val("");
        $("#inp-salad").val("");
        $("#inp-donGia").val("");

    };
    function getObjectRequest(paramObjectRequest) {
        "use strict"
        paramObjectRequest.trangThai = $("#select-trang-thai-don-hang").val();
    }
    function validateObjectRequest(paramObjectRequest) {
        "use strict"
        if (paramObjectRequest.trangThai === "") {
            alert("Hãy chọn trạng thái cho đơn hàng!")
            return false;
        }
        return true;
    }
    //Hàm tải dữ liệu menu đơn hàng lên form modal
    function loadInforMenuToForm() {
        $("#inp-duongKinh").val(gSelectedMenuStructure.duongKinhCM);
        $("#inp-suon").val(gSelectedMenuStructure.suonNuong);
        $("#inp-salad").val(gSelectedMenuStructure.saladGr);
        $("#inp-soLuongNuoc").val(gSelectedMenuStructure.drink);
        $("#inp-donGia").val(gSelectedMenuStructure.priceVND);
    }
    //Hàm tải thông tin đơn hàng thành công lên modal
    function loadInforMenuToFormModalComplete(paramObject) {
        "use strict"
        $("#inp-maDonHang").val(paramObject.orderId);
        $("#txt-infor-user").text(
            "Họ và tên: " + paramObject.hoTen
            + ".Email :" + paramObject.email
            + ".Số điện thoại: " + paramObject.soDienThoai
            + ".Địa chỉ: " + paramObject.diaChi
            + ".Lời nhắn: " + paramObject.loiNhan
        );
        $("#txt-infor-order").text(
            "MenuCombo: " + paramObject.kichCo
            + ".Đường kính pizza: " + paramObject.duongKinh
            + ".Số lượng sườn nướng: " + paramObject.suon
            + ".Salad: " + paramObject.salad
            + ".Số lượng nước: " + paramObject.soLuongNuoc
            + ".Loại nước uống: " + paramObject.idLoaiNuocUong
            + ".Loại Pizza: " + paramObject.loaiPizza
            + ".Phần trăm giảm giá: " + gSelectedMenuStructure.discount
            + ".Thành tiền: " + paramObject.thanhTien
        );
    }
    //Hàm xử lý sự kiện khi bấm út đồng ý trong modal complete
    function onBtnAcceptCompleteClick() {
        "use strict"
        $("#complete-modal").modal("hide");
    }
});